#include <iostream>
using namespace std;

int add(int f, int n)
{
    return f + n;
}

int main()
{
    int a;
    int b;

    cout << "Enter the two numbers: ";
    cin >> a >> b;

    int result  = add(a, b);

    cout << "The result of the addition: " << result << endl;

    return 0;
}
